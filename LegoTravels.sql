create database LegoTravel;

use Legotravel;

create table PASSENGER
	(Passenger_name varchar(20), 
    Category           varchar(20),
    Gender             varchar(20),
    Boarding_City      varchar(20),
	Destination_City   varchar(20),
    Distance           int,
    Bus_Type           varchar(20)
    
	);

create table PRICE(
	Bus_Type   varchar(20),
	Distance   int,
	Price      int
);

insert into passenger values('Sejal','AC','F','Bengaluru','Chennai',350,'Sleeper');
insert into passenger values('Anmol','Non-AC','M','Mumbai','Hyderabad',700,'Sitting');
insert into passenger values('Pallavi','AC','F','panaji','Bengaluru',600,'Sleeper');
insert into passenger values('Khusboo','AC','F','Chennai','Mumbai',1500,'Sleeper');
insert into passenger values('Udit','Non-AC','M','Trivandrum','panaji',1000,'Sleeper');
insert into passenger values('Ankur','AC','M','Nagpur','Hyderabad',500,'Sitting');
insert into passenger values('Hemant','Non-AC','M','panaji','Mumbai',700,'Sleeper');
insert into passenger values('Manish','Non-AC','M','Hyderabad','Bengaluru',500,'Sitting');
insert into passenger values('Piyush','AC','M','Pune','Nagpur',700,'Sitting');

select * from passenger;

insert into price values('Sleeper',350,770);
insert into price values('Sleeper',500,1100);
insert into price values('Sleeper',600,1320);
insert into price values('Sleeper',700,1540);
insert into price values('Sleeper',1000,2200);
insert into price values('Sleeper',1200,2640);
insert into price values('Sleeper',350,434);
insert into price values('Sitting',500,620);
insert into price values('Sitting',500,620);
insert into price values('Sitting',600,744);
insert into price values('Sitting',700,868);
insert into price values('Sitting',1000,1240);
insert into price values('Sitting',1200,1488);
insert into price values('Sitting',1500,1860);

select * from price;
select count(*) as female_600 from passenger where gender="F" and distance>=600;
select count(*) as male_600 from passenger where gender="M" and distance>=600;

select min(price) as min_price_sleeper from price where bus_type="Sleeper";

select passenger_name from passenger where passenger_name like 'S%';

select p.passenger_name,p.boarding_city,p.destination_city,p.bus_type,pr.price from passenger p join price pr on pr.bus_type=p.bus_type group by p.passenger_name;

select p.passenger_name,pr.price from passenger p join price pr on p.bus_type=pr.bus_type and p.bus_type="Sitting" and p.distance=1000;

select price from price where distance=600 and bus_type="Sleeper";
select price from price where bus_type="Sitting" and distance=600;

select distance from passenger group by distance having count(*)=1;

select passenger_name ,((distance*100))/(select sum(p.distance) from passenger p) as percent_traveled from passenger group by distance;

DELIMITER //
create procedure count_of_sleeper()
BEGIN
select count(passenger_name) passenger_in_sleeper from passenger where bus_type="Sleeper";
END //
DELIMITER ;
CALL count_of_sleeper();

create view AC as select passenger_name,category,gender,distance, Boarding_City , Destination_City,Bus_Type from passenger where category="AC";
select * from AC;

select * from passenger limit 5;

create database Lego_Solutions;

use Lego_Solutions;

create table COMPANY_TABLE ( project_code varchar(6),project_name varchar(20),project_manager varchar(20),project_budget varchar(10), primary key(project_code));

desc 	COMPANY_TABLE;

insert into COMPANY_TABLE values("PC010","Reservation System","Mr.Ajay","120500");
insert into COMPANY_TABLE values("PC011","HR Systems","Ms.Charu",500500);
insert into COMPANY_TABLE values("PC012","Attendance System","Mr.Rajesh",710700);

select * from COMPANY_TABLE;

create table employees (emp_no varchar(5), emp_name varchar(20), dept_no varchar(5),dept_name varchar(20), hourly_rate decimal(5,2),primary key(emp_no));

desc employees;

insert into employees values("S100","Mohan","D03","Database",21.00);
insert into employees values("S101","vipul","D02","Testing",16.50);
insert into employees values("S102","Riyaz","D01","IT",22.00);
insert into employees values("S103","Pavan","D03","Database",18.50);
insert into employees values("S104","Jitendra","D02","Testing",17.00);
insert into employees values("S315","Pooja","D01","IT",23.50);
insert into employees values("S137","Rahul","D03","Database",21.50);
insert into employees values("S218","Avneesh","D02","Testing",15.50);
insert into employees values("S109","Vikas","D01","IT",20.50);

select * from employees;

 create table department(dept_no varchar(5),dept_name varchar(20),primary key(dept_no));
 insert into department values("D01","IT");
 insert into department values("D02","Testing");
 insert into department values("D03","Database");
 
 select * from department;
 
create table Lego_employees(emp_no varchar(5), emp_name varchar(20),hourly_rate decimal(5,2),primary key(emp_no));


insert into Lego_employees values("S100","Mohan",21.00);
insert into Lego_employees values("S101","vipul",16.50);
insert into Lego_employees values("S102","Riyaz",22.00);
insert into Lego_employees values("S103","Pavan",18.50);
insert into Lego_employees values("S104","Jitendra",17.00);
insert into Lego_employees values("S315","Pooja",23.50);
insert into Lego_employees values("S137","Rahul",21.50);
insert into Lego_employees values("S218","Avneesh",15.50);
insert into Lego_employees values("S109","Vikas",20.50);

select * from Lego_employees;

create table payslips (emp_no varchar(5),hourly_rate decimal(5,2));

desc payslips;

alter table payslips add primary key(emp_no);

insert into payslips values("S100",21.00);
insert into payslips values("S101",16.50);
insert into payslips values("S102",22.00);
insert into payslips values("S103",18.50);
insert into payslips values("S104",17.00);
insert into payslips values("S315",23.50);
insert into payslips values("S137",21.50);
insert into payslips values("S218",15.50);
insert into payslips values("S109",20.50);

select * from payslips;